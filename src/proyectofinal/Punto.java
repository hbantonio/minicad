package proyectofinal;

import java.awt.*;

public class Punto extends Point implements Dibujable {

  private int radio; 
  private Color color;

  public Punto(int x, int y, int radio, Color color) {
    super(x, y);
    this.radio = radio;
    this.color = color;
  }

  public Punto() {
    this(0, 0, 0, Color.BLACK);
  }


  @Override
  public void dibujar(Graphics g) {
  g.setColor(getColor());
  g.fillOval(x - getRadio(), y - getRadio(), 2 * getRadio(), 2 * getRadio());
  g.setColor(Color.BLACK);
  g.drawOval(x - getRadio(), y - getRadio(), 2 * getRadio(), 2 * getRadio());
  }

    /**
     * @return the radio
     */
    public int getRadio() {
        return radio;
    }

    /**
     * @param radio the radio to set
     */
    public void setRadio(int radio) {
        this.radio = radio;
    }

    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }
}
