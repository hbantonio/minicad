package proyectofinal;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JOptionPane;

public final class Principal extends javax.swing.JFrame {
    
     protected Poligono original;   //Figura Original
     protected Poligono nueva;      //Figura Nueva con Transformacion
     Boolean bandera = false;       
    //Bandera para identificar si previamente selecciono la figura a aplicar transformacion
    public Principal() {
        initComponents();        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cuadrado = new javax.swing.JButton();
        Escalado = new javax.swing.JButton();
        rectangulo = new javax.swing.JButton();
        Translacion = new javax.swing.JButton();
        triangulo = new javax.swing.JButton();
        Rotacion = new javax.swing.JButton();
        linea = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Transformación Ricardo Antonio Hernandez Bravo");
        setBackground(new java.awt.Color(0, 102, 102));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jPanel1.setBackground(new java.awt.Color(0, 51, 153));

        cuadrado.setBackground(new java.awt.Color(204, 204, 204));
        cuadrado.setFont(new java.awt.Font("Berlin Sans FB", 0, 14)); // NOI18N
        cuadrado.setForeground(new java.awt.Color(0, 153, 0));
        cuadrado.setText("Cuadrado");
        cuadrado.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        cuadrado.setRequestFocusEnabled(false);
        cuadrado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cuadradoActionPerformed(evt);
            }
        });

        Escalado.setBackground(new java.awt.Color(204, 204, 204));
        Escalado.setFont(new java.awt.Font("Swis721 Hv BT", 0, 14)); // NOI18N
        Escalado.setForeground(new java.awt.Color(0, 153, 0));
        Escalado.setText("Escalado");
        Escalado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EscaladoActionPerformed(evt);
            }
        });

        rectangulo.setBackground(new java.awt.Color(204, 204, 204));
        rectangulo.setFont(new java.awt.Font("Swis721 Hv BT", 0, 14)); // NOI18N
        rectangulo.setForeground(new java.awt.Color(0, 153, 0));
        rectangulo.setText("Rectángulo");
        rectangulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rectanguloActionPerformed(evt);
            }
        });

        Translacion.setBackground(new java.awt.Color(204, 204, 204));
        Translacion.setFont(new java.awt.Font("Swis721 Hv BT", 0, 14)); // NOI18N
        Translacion.setForeground(new java.awt.Color(0, 153, 0));
        Translacion.setText("Translación");
        Translacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TranslacionActionPerformed(evt);
            }
        });

        triangulo.setBackground(new java.awt.Color(204, 204, 204));
        triangulo.setFont(new java.awt.Font("Swis721 Hv BT", 0, 14)); // NOI18N
        triangulo.setForeground(new java.awt.Color(0, 153, 0));
        triangulo.setText("Triángulo");
        triangulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trianguloActionPerformed(evt);
            }
        });

        Rotacion.setBackground(new java.awt.Color(204, 204, 204));
        Rotacion.setFont(new java.awt.Font("Swis721 Hv BT", 0, 14)); // NOI18N
        Rotacion.setForeground(new java.awt.Color(0, 153, 0));
        Rotacion.setText("Rotación");
        Rotacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RotacionActionPerformed(evt);
            }
        });

        linea.setBackground(new java.awt.Color(204, 204, 204));
        linea.setFont(new java.awt.Font("Swis721 Hv BT", 0, 14)); // NOI18N
        linea.setForeground(new java.awt.Color(0, 153, 0));
        linea.setText("Línea");
        linea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lineaActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Swis721 Hv BT", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 0));
        jLabel1.setText("Figura:");

        jLabel2.setFont(new java.awt.Font("Swis721 Hv BT", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 204, 0));
        jLabel2.setText("Transformación:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(cuadrado, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(rectangulo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(linea, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(triangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(407, 407, 407)
                        .addComponent(jLabel2)))
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Rotacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Escalado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Translacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(90, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(Translacion)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rectangulo)
                    .addComponent(Escalado)
                    .addComponent(linea)
                    .addComponent(triangulo)
                    .addComponent(cuadrado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Rotacion)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 507, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cuadradoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cuadradoActionPerformed
        // TODO add your handling code here:
        this.crearCuadrado();   
        bandera = true;    //bandera sera igual a verdadero cuando se cree el cuadrado
        nueva = null;      // nueva sera igual a null porque aun no hay una nueva figura con transformacion
    }//GEN-LAST:event_cuadradoActionPerformed

    private void rectanguloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rectanguloActionPerformed
        // TODO add your handling code here:
        this.crearRectangulo();
         bandera = true;
         nueva = null;
    }//GEN-LAST:event_rectanguloActionPerformed

    private void trianguloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trianguloActionPerformed
        // TODO add your handling code here:
        this.crearTriangulo();
         bandera = true;
         nueva = null;
    }//GEN-LAST:event_trianguloActionPerformed

    private void EscaladoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EscaladoActionPerformed
        // TODO add your handling code here:
        if(bandera==true){  //si previamente se creo la figura se puede aplicar la transformacion
         this.aplicarEscalado((float) 1.8);
        }
        else 
        { JOptionPane.showMessageDialog(rootPane,"Elige una Figura");
        }
    }//GEN-LAST:event_EscaladoActionPerformed

    private void TranslacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TranslacionActionPerformed
        // TODO add your handling code here:
        if(bandera==true){
        this.aplicarTranslacion(300,100);
        }
        else
        {
        JOptionPane.showMessageDialog(rootPane,"Elige una Figura");
        }
        
    }//GEN-LAST:event_TranslacionActionPerformed

    private void RotacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RotacionActionPerformed
        // TODO add your handling code here:
        if(bandera==true){
        this.aplicarRotacion(40);  //Se aplica una rotación a 40 grados
         }
          else
        {
        JOptionPane.showMessageDialog(rootPane,"Elige una Figura");
        }
        
    }//GEN-LAST:event_RotacionActionPerformed

    private void lineaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lineaActionPerformed
        // TODO add your handling code here:
        this.crearLinea();
        bandera =true;
        nueva = null;
    }//GEN-LAST:event_lineaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Escalado;
    private javax.swing.JButton Rotacion;
    private javax.swing.JButton Translacion;
    private javax.swing.JButton cuadrado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton linea;
    private javax.swing.JButton rectangulo;
    private javax.swing.JButton triangulo;
    // End of variables declaration//GEN-END:variables


public void crearCuadrado() {
    //System.out.println("CREAR CUADRADO");
   
    int[] pX = {200,350,350,200};
    int[] pY = {50,50,200,200};
    Puntos puntos = new Puntos();
    for (int i = 0; i < pX.length; i++) {
      puntos.add(new Punto(pX[i], pY[i], 2, Color.BLACK));
    }
    original = new Poligono(puntos, Color.MAGENTA);  
    repaint();
    }
  
  public void crearTriangulo() {
    
   // System.out.println("CREAR TRIANGULO");
    int[] pX = {250,150,350};
    int[] pY = {50,150,150};
    Puntos puntos = new Puntos();
    for (int i = 0; i < pX.length; i++) {
      puntos.add(new Punto(pX[i], pY[i], 2, Color.BLACK));
    }
    original = new Poligono(puntos, Color.YELLOW);  
    repaint();
  }
  
    public void crearRectangulo() {
   // System.out.println("CREAR RECTANGULO");
                                   //    X , Y
    int[] pX = {200,500,500,200};  // ( 200,50)
    int[] pY = {50,50,200,200};
    Puntos puntos = new Puntos();
    for (int i = 0; i < pX.length; i++) {
      puntos.add(new Punto(pX[i], pY[i], 2, Color.BLACK));
    }
    original = new Poligono(puntos, Color.RED);  
    repaint();
    
  }
  
    
    public void crearLinea() {
    //System.out.println("CREAR LINEA");
    int[] pX = {150,450};
    int[] pY = {50,50};
    Puntos puntos = new Puntos();
    for (int i = 0; i < pX.length; i++) {
      puntos.add(new Punto(pX[i], pY[i], 2, Color.BLACK));
    }
    original = new Poligono(puntos, Color.DARK_GRAY); 
    repaint();    
  }


  public void aplicarTranslacion(double dX, double dY) {
     Puntos puntos = new Puntos();
      for (Punto punto : original.getPuntos()) {
          int xOriginal = punto.x;
          int yOriginal = punto.y;
          int xNueva = (int) (xOriginal + dX);
          int yNueva = (int) (yOriginal + dY);
          puntos.add(new Punto(xNueva, yNueva, 2, Color.BLACK));
      }
      nueva = new Poligono(puntos, Color.BLUE);   
      repaint();
  }

  public void aplicarRotacion(double angulo) {
    double rad= Math.toRadians(angulo);
    Puntos puntos = new Puntos();
      for (Punto punto : original.getPuntos()) {
          int xOriginal = punto.x;
          int yOriginal = punto.y;
          int xNueva = (int) (xOriginal* Math.cos(rad) - yOriginal *Math.sin(rad));
          int yNueva = (int) (xOriginal* Math.sin(rad) + yOriginal *Math.cos(rad));
          puntos.add(new Punto(xNueva, yNueva, 2, Color.BLACK));
      }
      nueva = new Poligono(puntos, Color.BLUE);   
      repaint();
  }

  public void aplicarEscalado(float factor) {
      Puntos puntos = new Puntos();
      for (Punto punto : original.getPuntos()) {
          int xOriginal = punto.x;
          int yOriginal = punto.y;
          int xNueva = (int) (xOriginal * factor);
          int yNueva = (int) (yOriginal * factor);
          puntos.add(new Punto(xNueva, yNueva, 2, Color.BLACK));
      }
      nueva = new Poligono(puntos, Color.BLUE);  
      repaint();
  }
  
     @Override
   public void paint (Graphics g) {
    super.paint(g);
    //System.out.println("DIBUJANDO");
    Graphics2D g2 = (Graphics2D) g;
    
    if(original != null){
    original.dibujar(g);
    if (nueva != null) {
      nueva.dibujar(g);    
      }
    }
   }
  
   
}
